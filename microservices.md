# Microservice Updates

## GET endpoints

- endpoints will need to be created for every type of data being written to the
  warehouse
- each endpoint will take in four query parameters:
    - updatedStart (inclusive)
    - updatedEnd (exclusive)
    - continuation (optional)
    - limit

## Query Example

> we can add some utils to neb-microservice to make it easier for implementation

with continuation:

```sql
SELECT *
FROM   patient
WHERE  ( updatedat > ?
         AND updatedat < ? )
    OR ( updatedat = ?
         AND id > ? )
ORDER  BY updatedat,
          id
LIMIT  ?
```

without continuation:

```sql
SELECT *
FROM   patient
WHERE  updatedat >= ?
   AND updatedat < ?
ORDER  BY updatedat,
          id
LIMIT  ?
```

## Questions

- should we add indexes on the updatedAt column?
- should we be reading from the read replica of the rds instances?

# Reports Updates

## Update/Create Zoom Data Users

- each tenant will have a zoom data user
- that zoom data user needs an attribute of the tenants mysql database
    - note: this db host cannot be the cluster internal dns name

## Looking up external rds host name

### Option 1 - from k8s

- registry stores the cluster internal
- we can lookup the external host name using aws/kubectl commands

### Options 2 - maintain a mapping

- update the deployment so reports or registry knows the external host name
- this can be a config file that is generated during the deployment

## Endpoints

- POST to update zoom data for a new cluster (to only run during promotion)

## Migrations

- we need a process to get the zoom data data sources/dashboards from the dev
  environment to the prod environment

## Questions

- should we be using the read replica for this connection

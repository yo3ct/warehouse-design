# Datapipeline Design

- [new warehouse microservice](microservice.md)
- [updates to existing microservices](microservices.md)
- [updates to registry](registry.md)
- [updates to reports microservice](reports.md)
- [updates in zoom data](zoom-data.md)

## High Level

- A new microservice (both sharded by tenant and normal)
- The new microservice will "Pull" data from the microservices
- Will use "upsert" technique for writing data

## New Microservice

### Responsibilities

- schedule importing data
- import data

Challenges:

- don't use kafka for job queuing (bullmq)
- needs to have a single database + database per tenant
    - can either make 2 microservices or enhance neb-microservice
- currently zoom data does not support mysql 8
    - this will make it so the warehouse dbs can't be in the same RDS instances
- how to handle recurring appointments

### Schedule Data Ingestion

### Ingestion Worker

### Endpoints

## Registry

- new sharded by tenant microservice
- might need to update create tenant if the rds instance has to be different
  mysql 8 issue

## Microservices

- new endpoints for getting updated data
    - might be able to implement some utils in neb-miroservice
    - want them to use a read replica of the rds instance

```mermaid
graph LR
  ms1(Microservice 1)
  msn(Microservice n)
  ms1 & msn --> wh(new warehouse)
  wh --> registry(registry)
  wh --> db1[(warehouse DB 1)] & dbn[(warehouse DB n)]
```

```mermaid
erDiagram
  ingestionStatus {
    string id
    string tenantId
    string type
    string status
    timestamp after
    timestamp before
    string continuation
  }
```

```mermaid
sequenceDiagram
  participant wh as warehouse cron job
  participant registry
  participant whdb as warehouse DB
  participant it as ingestion topic
  wh ->> +registry:GET all tenants
  registry -->> -wh:tenants
  wh ->> +whdb:SELECT * FROM ingestionStatus
  whdb -->> -wh:ingestionStatuses
  wh ->> wh:add statuses if not exist for tenant
  wh ->> wh:filter out status that are not complete
  loop each ingestionStatus
    Note right of wh:TODO
    wh ->> +whdb:update status
    whdb ->> -wh:complete
    wh -->> it:produce ingestion message
  end
```

```mermaid
sequenceDiagram
  participant wh as warehouse consumer
  participant whDb as warehouse DB
  participant ms as microservice
  participant whTenantDb as tenant warehouse DB
  wh ->> +whDb:SELECT * FROM ingestionStatus WHERE id=?
  whDb -->> -wh:ingestionStatus
  wh ->> +ms:GET ?windowStart=&windowEnd=&continuation=
  ms -->> -wh:updated records
  wh ->> +whTenantDb:INSERT INTO stagingTable
  Note left of whTenantDb:Bulk INSERT
  whTenantDb -->> -wh:complete
  Note right of wh:Expand Relationships
  loop each expanded data type
    wh ->> +whTenantDb:UPDATE stagingTable
    Note left of whTenantDb:bulk UPDATE
    whTenantDb -->> -wh:complete
  end
  Note right of wh:Update external relationships
  loop each external relationships
    wh ->> +whTenantDb:UPDATE externalTable
    Note left of whTenantDb:bulk UPDATE selecting stagingTable
    whTenantDb -->> -wh:complete
  end
  Note left of wh:TODO<br/>how to handle aggregates?
```

## Reports Microservice

## Zoom Data

## Warehouse Tables

## Diagrams

```mermaid
graph LR
  ms1(Microservice 1) & msn(Microservice n) --> db1[(DB 1)] & dbn[(DB n)]
  db1 & dbn ----> dez(debezium)
  subgraph datapipeline
    dez --> tt1[[table topic 1]] & ttn[[table topic n]]
    tt1 & ttn --> funnel(funnel)
    funnel --> firehose[[firehose topic]]
    firehose --> warehouse(warehouse)
    warehouse <--> redshift[(redshift)]
  end
```

```mermaid
graph TB
  subgraph Cluster
    ms1(microservice 1)
    msn(microservice n)
    debezium(debezium)
    kafka[[kafka]]
  end
  subgraph RDS
    db1[(db 1)]
    dbn[(db n)]
  end
  subgraph rs [Redshift]
    redshift[(Redshift)]
  end
```

```mermaid
sequenceDiagram
  microservice ->> mysql:INSERT/UPDATE/DELETE
  mysql -->> mysql:binlog
  mysql -->> debezium:read binlog
  debezium -->> kafka:write message
  kafka ->> +funnel:consume alter message
  funnel ->> +registry:lookup tenant info
  registry -->> -funnel:tenant info
  funnel ->> funnel:decorate message
  funnel -->> kafka:produce firehose message
  funnel -->> -kafka:message consume complete
  kafka ->> +warehouse:consume firehose message
  warehouse ->> +redshift:look things up
  redshift -->> -warehouse:here you go
  warehouse ->> +redshift:UPSERT row
  redshift -->> -warehouse:complete
```

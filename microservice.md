# Warehouse Microservice Design

## Responsibilities

- schedule importing data
- import data from microservices
- transform data
    - expand related columns (id to value columns)
    - calculate values (patient balance)
- control data flow
    - ability to "replay" data
    - check status of data flow

## Challenges / Questions

- don't use kafka for job queuing (bullmq)
- needs to have a single database + database per tenant
    - can either make 2 microservices or enhance neb-microservice
- currently zoom data does not support mysql 8
    - this will make it so the warehouse dbs can't be in the same RDS instances
- how to handle recurring appointments
    - we need to explode the instance appointments
    - we will need to update instance appointments when changes are made
- how to handle a possible race condition of new data in a staging table not
  begin updated when a dependent row is updated?
    - example
        - new appointment comes in is in staging table
        - patient for that appointment is updated and updates live appointment table
        - new appointment gets merged into live table without the patient update
    - we can do a "chain" and only put one job on the queue for a tenant at once
- how do we create tenant sharded databases when system is first started up?
    - this might be handled by the migrate kafka message for each tenant
- what do we do when a status row is in an error state?
    - do we need retry logic? (I think so)
    - if x retries fail what do we do? (must be a data problem)
- do we need an cluster initialization process?
    - when promoting the new cluster may need to reprocess some data (or does it?)
- timezone sux!
    - we need to handle the practices timezone
    - we need to handle location timezone
- graceful shutdown of a pod
    - if there are current jobs running we want to finish those before stopping
    - bullmq has a pause function that we can use but will have to capture the
      kill signal
- sequelize and staging tables - need a way to get a sequelize model for
  staging tables

## Schedule Data Ingestion

A cron job will kick off ever x time and do the following:

- request all tenants from registry
- create status rows that don't exist
    - this can happen for a new tenant
    - this can happen for a new data type
- query own db for status records where:
    - status is complete
    - _filter out complete rows with delay?_
- for each complete status row
    - update the status row to be "queued"
    - update the status row with the "next" chunk to process
        - start = current end
        - end = now - 1 second
        - clear the continuation value
  - put job on queue

## Ingestion Worker

There will be a worker type per table. When a worker kicks off it will do the
following:

- verify the job still needs to happen (check db for queued state)
- request data from a microservice
    - send the start, end, continuation and page size to get the data
- drop staging table if exists (might exist in error cases)
- create a staging table
- bulk insert into that table
- for each dependency
    - bulk update all dependent columns in staging table
- for each depends on self
    - bulk update any tables/columns that depend on this data
- merge staging table into live table
- drop staging table

### Strategies for bulk updates

#### Dependent Expanded Columns

example of expanding patient name on the appointment record when processing
updated appointments:

```sql
UPDATE stagingAppointments
       LEFT JOIN patient
              ON stagingAppointments.patientId = patient.id
SET    stagingAppointments.firstName = patient.firstName,
       stagingAppointments.lastName = patient.lastName
```

example of expanding patient name on the appointment record when processing
updated patients:

```sql
UPDATE appointment
       INNER JOIN stagingpatient
               ON stagingpatient.id = appointment.patientid
SET    appointment.firstname = patient.firstname,
       appointment.lastname = patient.lastname
```

#### Dependent Calculations (Aggregates)

example of calculating the patient balance when there is a patient update:

```sql
UPDATE stagingPatient
SET    stagingPatient.balance = ( (SELECT Sum(credit.value)
                                   FROM   credit
                                   WHERE  credit.patientId = stagingPatient.id)
                                  - (
                                         SELECT Sum(debit.value)
                                         FROM   debit
                                         WHERE
                                         debit.patientId = stagingPatient.id) )
```

exmaple of calculating the patient balance when there is a credit update:

```sql
UPDATE patient
       INNER JOIN stagingCredit
               ON stagingCredit.patientId = patient.id
SET    patient.balance = ( (SELECT Sum(credit.value)
                            FROM   credit
                            WHERE  credit.patientId = patient.id) -
                                  (SELECT
                                  Sum(debit.value)
                                                                     FROM
                                  debit
                                                                     WHERE
                                  debit.patientId = patient.id) )
```

alternative?:

```sql
UPDATE patient
SET    patient.balance = ( (SELECT Sum(credit.value)
                            FROM   credit
                            WHERE  credit.patientId = patient.id) -
                                  (SELECT
                                  Sum(debit.value)
                                                                     FROM
                                  debit
                                                                     WHERE
                                  debit.patientId = patient.id) )
WHERE  patient.id IN (SELECT DISTINCT patientId
                      FROM   stagingCredit)
```

#### Merging Staging Updates

in a transaction:

```sql
DELETE FROM patient
USING stagingPatient
WHERE patient.id = stagingPatient.id;

INSERT INTO patient
SELECT * FROM stagingPatient;
```

### Tables

- allocation
- appointment
- appointment_status_fact
- appointment_type
- audit_log
- batch
- campaign
- comm_history
- communication
- credit
- date_dimension
- debit
- encounter
- encounter_charge
- encounter_diagnosis
- line_item
- line_item_debit
- merchant_account
- message
- migrationmetadata
- patient
- patient_package
- payment
- payment_method
- payment_plan
- practice_user
- recurrence_event
- recurring_payment
- refund_payment
- relative_date_dimension
- schedule
- template
- tenant
- void_payment

## Endpoints

- check status of all tenants (show non complete?)
- check status for a tenant
- update status to "reprocess" data
- check status of the job queue

## Audit Logs

- audit logs are already being produced on a kafka topic
- need to implement a consumer to write to an audit table in the sharded
  database
- we may need to copy current audit logs into the sharded table (redshift to
  mysql)
- will need to expand some information (patient name)
- will be difficult to handle bulk data here

## Motivations

- bulk is important!
    - in worst case scenarios we are dealing with conversion customers (all data
      all at once)
    - updating single rows is slow
    - upsert is slow
- this data will end up in a single warehouse (redshift?)
    - this means we want the data in these warehosue dbs to be the exact same
      as what will be written to the single warehouse (no transformation)
- the warehosue schema needs to be as simple as possible
    - ability for reports to have no joins!
    - non technical people (and eventually customers) will be writing reports
      for this data
- the data capture will be ever evolving and a lot of different data being
  captured
    - for new data we want to it to be just configuration as much as possible
    - complex dependency management can be abstracted into reusable parts that
      can be configured
- integration testing is very important
